pip install --upgrade poetry pre-commit
rm -rf dist
poetry build

# ARTIFACT=qary-0.7.5
export ARTIFACT=$(poetry version | sed "s/ /-/g") \
    && tar xvOf "dist/$ARTIFACT.tar.gz" "$ARTIFACT/setup.py" > setup.py


VERSION=$(echo $ARTIFACT | cut -c6-)  # version number is after "qary-" in the artifact str
MESSAGE="Updated setup.py to match dependencies and version number ($VERSION) in pyproject.toml"

echo
echo "=================================================================="
echo 'echo $MESSAGE'
echo
echo $MESSAGE
echo "=================================================================="
echo

echo "=================================================================="
echo 'git commit -am $MESSAGE'
echo
git commit -am "$MESSAGE"
echo "=================================================================="

echo "=================================================================="
echo 'git tag -a $VERSION -m "$MESSAGE" ...'
echo
git tag -a $VERSION -m "$MESSAGE"
echo "=================================================================="

echo "=================================================================="
echo "git push && git push --tag ..."
echo
git push
git push --tag
echo "=================================================================="
