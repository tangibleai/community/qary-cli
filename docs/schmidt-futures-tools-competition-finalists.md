# Schmidt Futures Tools Competition Finalists

1. [Small](#catalyst-prize-finalists)
2. [Medium](mid-range-prize-finalists)
3. [Large](large-prize-finalists)


## Catalyst Prize Finalists

### An Adaptive and Interactive Tool to Enhance Learning Mathematics through Fun

#### Team: Messamrt IT Solutions

  We propose to develop an innovative, holistic, and interactive mathematics tool that allows integration of school curriculum and related real-life applications for middle-school students in Ethiopia. With an engaging and interactive design, it will help students to ”learn mathematics through fun”. We plan to design an integrated user interface for teachers, students, and parents in the proposed tool with a dashboard and data acquisition functionalities. The data acquisition functionality will promote research on both students learning and further advancement of the tool proposed using system collected data. The dashboard will be used to display different automated data analysis results for student progress monitoring. Using the data collected from students, the tool will incorporate a machine learning model to embody automatic and intelligent suggestions during student interaction. Moreover, the functionalities of the proposed tool will be designed based on a reusable application programming interface (API) approach so that the implemented services can be integrated into existing systems.

### Authentic Math Problems with Multiple Approaches and Solutions

#### Team: Jeff King

  Many current math platforms only offer problems that accept a single correct answer. While this can be effective at measuring mastery of a single standard, it can also reinforce the perception that math is a rigid subject where each problem only has one correct approach and answer. To show students that solving real world math problems is an engaging and collaborative process, I hope to create a platform with well designed math problems that allow students to propose different but equally valid answers. On each problem, the platform will also ask students to compare their approach with those of other students and provide opportunities to give feedback to other students or modify their own solution. With more authentic problems, I hope students will gain improved attitudes towards math and as a result, be more likely to be on grade level by the time they graduate middle school.

  The platform will provide the following pieces of data to teachers and researchers: (1) Every iteration of a student’s answer on the problem, (2) student explanations as to how they solved the problem, (3) Feedback the student received, and (4) feedback the student provided to other students. In addition, teachers and researchers can modify certain aspects of the problems to compare whether different variations affect the type of approaches students use to solve each problem.

### Automatically Assessing Text Readability

#### Team: Joon Suh Choi and Scott Crossley

  Our solution is the Automated Readability Tool for English (ARTE). ARTE is a free, easy-to-use, and accessible natural language processing (NLP) tool that automatically measures text readability. The current prototype (a standalone desktop version) calculates nine different readability formulas. Our main goal is to port this tool to an online environment and scale it to make it more accessible to teachers, administrators, researchers, and material developers. Additionally, we plan on adding new functions such as a file management system with a graphic user interface (GUI), data visualization, and expanding the number of readability formulas supported by the program. We envision that these solutions will help users generate valuable data about text readability that can lead to practical gains in student reading skills.

### Booklity

#### Team: Kube Industries

  Booklity is a web app that students can use to ask questions that they are struggling with and then get assistance from other users. By using data analysis to evaluate how a user engages on the platform and find out whether they need more assistance with a particular subject. This will help schools and departments adjust their curriculums to cater for all students. It is a theory that students can learn better and quicker if they get assistance in an easygoing manner.

  Finding out what a student is struggling with should be done very early on not during/after exams. The findings will be used to provide the user with more personalised tools (tutorials and college/university accredited tutors) to improve where they are lacking.

### Dash: An Evidence Driven Teacher Superpower

#### Team: Aditi Mallavarapu, Elham Beheshti and Lydia Tse

  In response to the inability to gauge students’ complex learning processes in physical classrooms aggravated by the large class sizes, and the recent shift of classrooms to the digital realm due to COVID-19, learning needs to be engineered by collecting and extracting nuances from students’ and teachers’ interactions to understand the impact of existing pedagogical practices on the student learning outcomes.

We propose a teacher facing learning engineering based tool, Dash, a web-based application that detects and tracks in real-time the actions and events (including students’ exploration markers, the teachers’ access to students’ information, and self-reported pedagogical changes) in block-based programming curricula and reports them on a dashboard interface. The purpose of our tool is to provide teachers the ability to experiment and measure impact of their pedagogical practices thus helping them adopt evidentially effective practices. The application has three core components, (1) a browser extension to collect the students’ while learning, (2) a browser extension to track teachers’ interactions while orchestrating and (3) a dashboard interface to display the collected information for teachers. Our goal is to specifically, imbue learning engineering practices (introspection and methodical experimentation) into the current CS teaching practices, collaborate with learning scientists to understand learning as a phenomena and identify best practices to increase student outcomes. Learning engineering would create a tight feedback loop by helping teachers engage in methodical experimentations driving continuous improvements.

### Early Numeracy Learning Intervention

#### Team: Ashna Saraf and Neil Vakharia

  A smart phone-based game that promotes the acquisition of developmentally appropriate early numeracy skills for preschoolers, giving parents/teachers a better understanding of areas of strength and improvement. Steeped in psychological research, the app will be a learning intervention designed to strengthen children's neurocognitive capacities such as visuo-spatial awareness; non-symbolic numerical skills, such as subitizing, sorting and identifying quantities; and symbolic numerical skills, such as counting and basic operations.

  Most early math skills are not developmental in nature and require effective teaching and scaffolding. Each skill has a specific progression, for example in the case of counting, children need to master 5 independent sub-skills before they can effectively count. Since preschoolers can’t articulate which sub-step they struggle with and early educators often don’t know what these subskills are, this game will work to develop each subskill, collecting response time and accuracy data, and build a personalized learning path for the child. The children’s performance data can be accessed by learning scientists looking to understand how foundational math skills develop and evaluate the most effective ways to teach as well as scaffold them. Moreover, the app itself will create personalized feedback loops for each child, reteaching principles the child needs to improve on in different ways and provide effective intervals between concepts to consolidate and reemphasize certain skills.

### Embedded Learning Assessment and Personalization Platform for Maximizing K-8 Math Learning

#### Team: Mark Rose Lewis and Percival G. Matthews

  We are building a platform that transforms students’ day-to-day math work into actionable views of their learning that teachers and interventionists can use to boost student learning and help fix key gaps caused by disruptions due to the COVID-19 pandemic. This tool takes the daily work that students are doing in classrooms and via distance learning and transforms it into a model of their current learning state through a combination of existing learning theory, computational modeling, and teachers’ expert judgments. It then presents these results to teachers and interventionists in real-time along with highlights of students’ strengths and weaknesses and suggestions for how new learning and recovered learning can be maximized.

### KnowLearning Open-Source Learning and Research Platform

#### Team: KnowLearning

  KnowLearning seeks to accelerate our open-source edtech infrastructure for customized learning sequences and progress reporting, and help middle school students understand fractions. The KnowLearning platform, currently in beta, aims to solve expensive and highly technical challenges commonly faced by digital education projects. These challenges include hosting, deployment, resource management, versioning, learner cohort management, access and permission management, data-collection, security, and reporting.

  The Award will fund the accelerated development of core features of KnowLearning’s open-source infrastructure, specifically (a) the ability for teachers and partnering researchers to assign custom learning sequences to individual learners or cohorts and (b) progress reporting on general student work or special assignments. The Award will also fund the development and release of a “test balloon” learning module targeting fractions. The module will be freely released, including open-source release of all components and technical assets. We chose fractions because our expertise in this area can help address this notoriously challenging and foundational mathematical content area, while also informing development on KnowLearning’s core architecture.

  KnowLearning’s open-source platform aims to increase collaboration and experimentation in Learning Engineering. We do this by streamlining common functions, reducing cost, and mitigating technical risk.

### "Math with +Meaning" Mobile App

#### Team: Meaning

  At Meaning we work to bring equity in math for all students, helping and supporting specially those who struggle the most. We developed digital solutions in Chile to engage low-income families in the learning process of their children through text messages and WhatsApp. Now, we want to develop a mobile application that can help us reach more students and support their academic performance in math. Furthermore, all the data collected through the app will be available in a website for researchers to start new academic studies that contribute to the science of learning and education.

  This app helps educators to connect math learning with family and daily life. With our app educators bring math to life with thousands of easy-to-achieve, short and fun activities, designed for parents to carry out with their children. Each at-home activity serves as an introduction for math lessons and can be connected to the curricular content in class, which provides a greater, more lasting, and meaningful learning experience for the students. We know we can help students and their families with this app. Let’s have fun doing math!

### My Storied Future: A Social-Emotional Learning Mobile Application to Increase Youth Pursuing Post-Secondary Education

#### Team: Rebecca Gorlieb and Madison Stanford

  To drive more students, especially low-income youth of color, to attend college via an identity-based intervention, My Storied Future, a mobile application for students and schools, presents students with curated, short documentary-style videos about inspiring adolescents and current events. Students engage in semi-structured reflections (written or spoken) about how each story makes them feel. Automated text analysis assesses in real-time the sophistication of students’ responses and directs the level of support provided in follow-up questions. After responding to several stories, students answer questions about their goals and values, with relevant text from story reflections prepopulated for efficiency to help them. Teachers or peers review students’ answers to facilitate discussions among users and thereby further refinement of their visions for the future. Possible physiological indices of deep engagement (including patterns of eye gaze, eye blink rate, pupil dilation, facial expressions, voice pitch, and speech breathiness) will be measured from audio and video recordings.

  Data from My Storied Futures will advance the science of learning by: (1) increasing understanding of demographic and developmental characteristics of adolescents’ ability to engage in abstract systems-level thinking; (2) identifying hidden, often subconscious, physiological indicators of abstract engagement to make future technology-based detection of abstract engagement better; and (3) possibly informing prototypes of classroom practices for teaching adolescents physiological regulatory patterns that support learning.

### PALS_Peer Assisted Learning Systems

#### Team: Project Set

  PALS (Peer Assisted Learning System) is a software application for middle school students to solve problems with peer support. It is intended to offer a reliable support network to students with inadequate learning support at home (e.g. first-generation students from under-privileged families), outside the classroom. The need for the tool has accentuated in the post-Covid world, within our target community.

  We will co-develop the AI-embedded tool through design sprints with a community of more than 1000 teachers and students in our partner schools in India. Post rollout, testing and refinement, we will offer the tool to a wider set of learning communities in India and abroad. The incremental users—and the consequent addition of solved problems in the solution bank—)will significantly increase the value and utility of the tool for all users in the community (“network effect”). In addition, the tool will include a module to allow research interventions and capture (non-confidential) data for analysis by the learning engineering community. In the initial instance, we plan to capture and analyze data on tool adoption, tool quality, student engagement and learning outcomes to better align the tool and pedagogical elements in the program for optimal impact.

### Podsie

#### Team: Podsie

  We’re proposing a new technology, Podsie, to improve student learning outcomes in K-12 classrooms. Podsie is a web app that improves student information retention by allowing students to review material via personalized flashcards that incorporate spacing, interleaving, and frequent retrieval. It also includes features that make it more practical to implement learning science in the classroom such as standard mastery tracking and teacher tools for student accountability. In addition to implementing this new technology, we evangelize these learning techniques to teachers who are not yet bought into the benefits of spacing, interleaving and frequent retrieval.

  As for learning engineering, Podsie tracks student performance metrics that can be filtered in many dimensions. We’ve built tools for researchers, teachers, and ourselves to query and drill down into this data in a time series format. We are also building an experimentation framework within Podsie to allow ourselves and researchers to conduct proper randomized controlled trials. Our data, along with this experimentation framework, enables us to implement learning science within Podsie and help researchers advance learning science by providing them with rich, real world student data.

### RecapCS: A Learning Tool to Increase Peer Collaboration and Deepen Reflection

#### Team: Ember Liu, Neil Thawani, and John Stamper

  Our goal in developing RecapCS was to help high school CS instructors in project-based courses better identify students’ problems and misconceptions, assess their learning, and lend support as-needed via a community forum. RecapCS enables high school computer science (CS) students and their instructors to have more meaningful and insightful learning experiences over time by serving as a central store of knowledge for project documentation.

  Our initial design of RecapCS was constructed so students check in with their instructors by setting learning goals, log their breakthrough moments, and track their progress over time while contributing knowledge and support to their online class communities. We also researched and prototyped a Virtual Community of Practice (VCoP) on the back-end of RecapCS for teachers to connect and exchange resources, ask and answer questions, and share their own learnings with their colleagues.

### Seeking Alpha

#### Team: Florida Atlantic Univeristy

  COVID-19 has forced schools to pivot to online education, creating entirely new challenges for teaching and learning. Students often lose interest in learning when isolated from their peers and instructors, and teachers providing distance learning cannot read their classrooms as accurately as in person. Likewise, whether in school or at home, teachers have difficulty monitoring engagement when students interact with online tools.

  This proposal focuses on a technological solution for monitoring and improving student engagement and subsequent learning outcomes. Named for a financial term for a strategy’s ability to yield excess return on investment, and indicative of FAU Schools’ commitment to accelerating learning for all students, Seeking Alpha will integrate eye-tracking technology and a user dashboard to identify low student engagement and guide teachers in improving their instruction and providing identified students with academic and non-academic supports.

  Researchers will be able to mine data from the platform for information related to student attention, student learning (demonstrated through assessment results), and teacher instruction, in order to conduct science of learning studies.

### Smart Games for Intelligent Tutors

#### Team: Beverly Woolf, Ivon Arroyo and Danielle Allessio

  We propose to develop Smart Games, new tools that integrate video games with existing intelligent tutors. The novelty is to combine game-like interventions (visual feedback, student control, rewards, agents, narrative) with existing intelligent tutors (personalization, adaptivity, modeling, rich learning gains). Although the idea of educational games is not new, most games currently on the market are unenjoyable, ineffective, or targeted at a younger audience (age 8 and below). Currently, more than 25% of students dropout of high school because they are bored. We will identify effective interventions and apply a uniform methodology to add gamification to several intelligent tutors. By making learning more fun and by personalizing these games we will increase learning and attitudes.

### STEM Video Game

#### Team: Ripples Studio

  Ripples Studios is on a mission to inspire all students, especially young girls and racialized groups, to consider a career in science, technology, engineering, or math (STEM). Using an educational video game, we aim to increase student comprehension, confidence, and engagement with STEM concepts and to improve diversity and inclusion within the STEM community. Our story-based approach will allow players to learn about various STEM careers and their crucial role in society by showing real-world applications. Diverse characters, equally represented in each of the careers, will empower the students to see themselves in these roles. We will lay the groundwork for students to build a vision of their future and inspire them to love STEM.

  The game will be developed using the Unity gaming engine which is a powerful off-the-shelf software that allows for rapid cross-platform development (web, Android, iOS, etc.) and has advanced data collection and analytics capabilities. Our game prototype will be completed by the beginning of 2021 and this award will accelerate the development of our data analytics infrastructure and research framework. This includes the construction of a web portal for external researchers to access the data and a form where researchers can propose questions. By design, our game is both modular and expandable. Each STEM career added will be a separate game component which will allow for highly customizable experimentation based on external research needs. By harnessing the success of digital game-based learning and leveraging Unity’s advanced analytics capabilities, our tool will provide unparalleled learning outcomes for students while delivering powerful insights for external researchers.

### TeacherPrints v2.0

#### Team: TeacherPrints

  Research shows that robust learning occurs when both teachers and students function as sources of new ideas (Schoenfeld, 2014; Institute for Research on Learning, 2011; Ford, 2012). What we understand less well is the journey that these new ideas take as learners integrate them into knowledge, and what factors might influence how students revise and refine their understanding of new ideas along the path to robust learning. For example, how do the proportions of teacher and student talk in a classroom affect the manner, speed, and effectiveness with which students take up and integrate new instructional concepts? Can we map the journey new ideas take through teacher-to-student and student-to-student talk to identify paths that are more likely to lead to deeper understanding? The answers to these questions have the powerful potential to provide teachers with the feedback they need to manage classroom conversations in ways that maximize student learning, and to give educational researchers insight into the features of classroom dialogue that facilitate positive learning outcomes in a diverse range of contexts.

  The suite of tools needed to find these answers are a work in progress. Our existing TeacherPrints platform translates audio recordings of classroom dialogue into accessible, actionable visualizations of speech activity (frequency, duration, overlaps, and pauses) by speaker type (teacher or student). By highlighting whose speech predominates within a lesson and characterizing classroom verbal interactions, it encourages teachers to create space for students' voices to be heard. This award will support the development of LearningFlow, a natural language processing (NLP) enhancement to the TeacherPrints platform that analyzes the content of those classroom conversations to trace how specific instructional concepts are communicated, explored, and refined through the teacher’s and students’ verbal interactions.

### TrackRead

#### Team: Vanavia Weatherly, Harry Kamdem and Valerie Cho

  This project seeks principally to develop an application in which users can practice reading, using finger tracking as an aid. Within the app, this shall mainly consist in highlighting word sections as they are read by the user (with their finger following along) and occasionally playing out any problematic syllable to help the user. It will be made possible by combining an interactive touch-based interface with AI-powered ASR (Automatic Speech Recognition) and eye-tracking technologies.

  The app shall provide an engaging and adaptive environment for readers, which will cater to each user's specific needs, and the data collected shall be made available for researchers to understand and continually improve the users’ proficiency.

### Using Multimodal Data to Prepare the Next Generation of Data Scientists

#### Team: Bertrand Schneider

  In an increasingly data-driven world, big data is infiltrating all aspects of our lives. As such, data literacy is becoming a central skill for the next generation of STEM students. However, most data science courses focus on pre-collected datasets and neglect challenges associated with data collection and data cleaning. This project is about teaching data science skills through real-world, ecological, multimodal datasets through a semester long course.In an increasingly data-driven world, big data is infiltrating all aspects of our lives. As such, data literacy is becoming a central skill for the next generation of STEM students. However, most data science courses focus on pre-collected datasets and neglect challenges associated with data collection and data cleaning. This project is about teaching data science skills through real-world, ecological, multimodal datasets through a semester long course.

  There are two technological contributions of this course. (1) A website that allows learners to collect rich multimodal datasets, from any webcam or video recording. While this data used to be accessible only through physical sensing devices (e.g., Tobii eye-trackers, Microsoft Kinects, Empatica E4), it has become possible to collect similar datasets by using machine learning algorithms that can track eye movements, body postures, heart rate and emotions from facial expressions. This recent development opens new doors for teaching data science, by letting learners collect rich multimodal datasets. (2) A second contribution of this project is a learning portal, which provides instructional videos to teach concepts related to multimodal analytics. This learning portal also captures multimodal data from learners (e.g., gaze orientation, emotional states, body postures) as they are watching videos. This data serves a double purpose: advance our understanding of students' state as they are learning from video material, and provide students with a personal dataset to analyze throughout the course.

### Write When Right

#### Team: Robert Comeau, Denise Patmon, Ava Yang-Lewis and Mark Lewis

  To center the whole student during this crisis of marginalization, we’re developing an online reflective journal for high school and undergraduate use. It connects students and educators for mutual growth through dialogue. Sentiment analysis will help guide writing and feedback, and shape effective practices, through rapid experimentation and data-driven design. Our connections to urban districts and national educator networks will help us scale. This award will support initial development of a dynamic platform for social and emotional learning (SEL) that will be widely available to students, teachers and researchers. This work will include building easy-to-use, secure interfaces, automated feedback suggestions that will help teachers better support their students’ growth, as well as their own, and an underlying data structure that will enable future development of the platform and external research.

  Therapeutic and narrative writing helps us de-stress, develop positive self-talk and author counter-narrative, combating external and internal marginalization. Reflective writing develops metacognition and self-regulated learning. Educators grow through reflection and dialogue, with careful coaching for antiracist practices and mindset change. As teachers grow in their capacities to see and serve the whole student, students will grow in the expanding spaces that center marginalized students.



## Mid-Range Prize Finalists

### Blank Page

#### Team: FineTune Learning and Tangible AI

  Blank Page™ takes a novel approach to addressing the persistent crisis in writing in K-12 by targeting students’ ​thinking​. It’s a pre-writing app that will use human-in-the-loop AI to mediate, scale, and capture the interactions between teacher and student that typically occur in one-on-one conferencing. Through a gamified user experience, students will respond to a series of increasingly individualized questions posed by their teacher that help them to catalyze, formulate, and organize their ideas around an assignment. The result will be a well-structured outline that reflects their own (critical) thinking -- not an algorithm’s -- and which they can then use to guide their actual writing.

  Writing theory has remained relatively stagnant for more than 30 years, even as student performance has declined. In addition to improving learning outcomes, Blank Page has the potential to drive new theory. To achieve this end, the app is being instrumented from the back end​ to be data-centric and fully parameterized, providing UX designers and researchers with limitless options for ​content ​experimentation -- a step beyond conventional (front-end) testing and analytics tools. Configurable with a GUI similar to best-in-class content-management systems, Blank Page will distribute and parallelize innovation and content creation, accelerating the improvement in learning outcomes.

### Boost: eLearning Assistant

#### Team: Boost App, Inc.

  Both pre- and post-COVID, students at all educational levels have increasingly been utilizing learning management systems (LMSes; e.g., Canvas) to carry out schoolwork online. While LMSes make it easy for teachers to post online assignments, quizzes, and resources, LMSes do not make it easy for students to manage the workload. With every new assignment comes a new deadline, and a new opportunity for students to fall behind. Given that submitting assignments on time is the strongest predictor of student success in online learning environments, Boost fills an important gap in the default LMS toolkit: providing real-time automated services that support student engagement and success, personalized to the needs of individual students. Experimental pilots at Indiana University have demonstrated that Boost’s support features improve assignment submission rates and course performance at scale. With this proposal, we aim to expand Boost’s services to support K12 learning environments, and to support integration with multiple LMSes, so that these benefits can have broader impacts.

### Chimple Class: Connecting Teachers and Children for Literacy and Numeracy LearningFlow

#### Team: Chimple Learning

  Chimple is a gamified mobile app for children aged 3-8 to self-learn reading, writing and maths in English, Hindi and Swahili (coming soon in other languages). The app is a top-5 finalist in the Global Learning XPRIZE. The app has been used in environments where no teachers are present (such as rural Tanzania), in schools (Mumbai Government Municipal Schools), communities and homes and has been downloaded by 24,000 children. The current COVID-19 related school closures have created a need for pre-school and primary school teachers to connect with their students at home through mobile. Teachers have currently resorted to sending activities by WhatsApp photo which does not engage the child nor help them learn in a structured way. The proposed Class tool will enable teachers to use the Chimple app and connect with their students through the app. It will serve as a dashboard for teachers to connect with their students (and their parents), monitor their students’ progress at home and assign relevant learning activities in the Chimple curriculum to the entire class or individual student. An online version of the same console can be used by educational institutions, NGOs, regional educational offices to track foundational learning progress for the children they are responsible for.

  The main learning engineering benefit we are deriving from adding the Class tool is to study lesson assignment by the teacher, either collectively to the class or individually to a student. We will mine this data to derive better algorithms for recommendation of lessons to improve both learning outcomes and engagement. In addition, we are also building a researcher’s console which will provide a workbench to conduct various experiments and analysis. Using the console, researchers can set up A/B tests, study effectiveness of various nudge strategies, measure engagement of children across different spectrums, get preliminary analysis on usage and download data for further data mining.

### CurrikiStudio - Free & Open Digital Content Authoring and Publishing Platform - Your Learning Content. Activated.

#### Team: Curriki

  CurrikiStudio is an open-source interactive digital content authoring and publishing solution that empowers users to create once and publish everywhere—drastically reducing technical and financial barriers to building and publishing great interactive learning experiences, providing unprecedented access and ease of use to content authoring and increasing the availability of great learning, anywhere.

  We believe that by helping educators create interactive, engaging lessons, CurrikiStudio has the unique opportunity to be on the forefront of learning engineering because of the robust data collection around learning design and the learner (end-user) experience with that. Our current system has deep measurement capabilities to demonstrate the learners’ experience with CurrikiStudio projects such as time on task, number of attempts, and frequency of use.Through this grant we plan to turn those data points into actionable insights that help all learning designers understand what it truly means to “create an engaging learning experience” and provide in-application prompts to ensure that the best practices of learning design are followed. We propose to do this by enhancing our current offering with robust learner-targeted qualitative surveys to actively ask students what they think of a lesson. This user-centered feedback, coupled with our quantitative data, will give CurrikiStudio the ability to build the capacity of educators and learning designers by equipping them with insight about what makes a lesson effective and engaging.

  Curriki will work with the Katalyst Methods research team to ensure data are easily and effectively leveraged for the purposes of better understanding the science of learning. As the leading Open Source education technology platform, it is our place in the ecosystem to develop the toolkit for: (1) authoring interactive learning experiences; (2) responsibly collecting data on learner engagement with the authored experiences; (3) enabling any researcher with the ability to derive meaningful trends about how learners are responding to creations of authors; and (4) iterating on our tool to incorporate learnings - and share those learnings and open source technology models with other education technologies so that everyone can benefit from the learning engineering of creating interactive learning experiences.

### Graspable Math Teams: A Collaborative Math Whiteboard

#### Team: Graspable Inc.

  Collaborative learning, problem solving, and discourse are crucial in math education. Yet remote instruction for mathematics during COVID19 is mostly limited to lecture-style lessons and students doing solo coursework. We propose a software solution to facilitate synchronous, collaborative math work online. The software will allow students to jointly solve algebraic problems on an interactive, supportive digital algebra whiteboard. We will design two different collaborative settings and automatically record rich interaction data to support iterative improvement of the software and enable researchers to explore student discourse and learning outcomes across the settings. Data will include replayable click-stream data, drawings, text messages, and audio-chat.

### Rising On Air

#### Team: Rising Academies and Filament AI

  Rising On Air was a 20-week program of free, downloadable, ready-to-air, radio scripts, audio and SMS content, covering foundational language arts and math for five age groups across K 12. Built and scaled in less than 150 days, the program was adopted by 35 partners in 25 countries including 6 governments, translated into 12 languages and reached upwards of 12 million children.

  This traction shows the transformational potential of instructional audio to supplement traditional text-based curriculum content in crisis settings. We want to unlock that potential by delivering content directly to users’ phones via an AI-powered chatbot. Focusing initially on English but with the potential to be expanded to include French, Arabic or other languages into which Rising On Air has been translated, our chatbot would enable users to attain grade-level mastery in language arts and math by accessing the right curriculum topics for their needs, in the right sequence, via both text and audio.

### Speak Agent Math+Language: Learning Engineering Component

#### Team: Speak Agent

  The goal of Speak Agent Math+Language is to increase the number of students achieving proficiency with Middle-Grades Math, as assessed by content, reasoning, and modeling items. The existing Speak Agent platform measures intermediate outcomes toward this goal: It tracks progress toward mastering the specific middle-grades math concepts targeted by a school district's curriculum. Speak Agent has moderate evidence of promise showing a 3X acceleration in STEM concept mastery—derived from its theory of change, research-based strategies, and product certifications in Research-Based Design and Learner Variability.

  This mid-range proposal aims to enhance Speak Agent with a Learning Engineering Component that creates a continuous feedback loop to iteratively improve course design. This will enable better in-app learning, PD, and dosage guidance by specifying the optimal mix, timing, and modes of use for the 32 research-based strategies embedded into our math product; tailoring strategies to each learning context; and ensuring consistently high fidelity of implementation.

### Student-Centered Tools for Data-Driven Pathways to Higher Ed Success

#### Team: MARi

  MARi is an artificial intelligence (AI) SaaS platform for academic and workplace skills. MARi (short for MARiana Trench and pronounced “mar + ee”) is capable of longitudinally tracking all the knowledge, skills, and abilities of a person throughout their entire life. The intelligent “whole-person” skill repository provides MARi the foundation to transform education for under-engaged and disengaged students by developing critical self-efficacy belief systems and enabling radical engagement in their futures.

  Our current educational system does not address a critical determining factor in educational success: readiness to learn. And by that, we aren’t talking about whether a student knows her ABCs when she enters kindergarten, we’re talking about a deeper question: Is the student motivated to learn? Our external researchers will use our built-in survey tool to determine how a student’s locus of control, self-efficacy, and world view impact their performance. In addition, they plan to track how a compelling vision for the future, with positive interventions along the way, impact a student’s motivation to perform. Our researchers are looking for the psychosocial influences that make outsized

### Transforming the Virtual Teacher Learning Experience to Accelerate K-12 Student Learning

#### Team: Teaching Lab

  How educators teach has fundamentally changed due to COVID-19. Educators were expected to leave their classrooms, engage students through tiny cameras, juggle dozens of tech platforms and apps, and be experts in the delivery of online instruction. It is still possible, however, to help teachers learn and improve their expertise in uncertain times.

  Teaching Lab, through their new 100% virtual teacher professional learning platform, Teaching Lab Plus, seeks to accelerate K-12 math and ELA student learning outcomes through transformational teacher learning experiences that ensure teachers equitably instruct and engage students in any context, including online and hybrid contexts.

  Since Teaching Lab Plus launched in March, we have 5,000 educators accessing the learning management system, reaching over 300,000 students. Through our system, we have collected hundreds of thousands of unanalyzed data points in our platform that could hold the key to understanding which teacher experiences lead to dramatic improvements in teacher and student learning.



## Large Prize Finalists

### AI-based Personalized Recommendation Engine for Open Video Content in Indian Languages

#### Team: Avanti Fellows

  We are building an AI-based platform that enables personalized learning through free interactive videos. The platform will (a) recommend high-quality free video content in local languages, (b) automatically build interactive lessons on top of these videos, (c) work on feature phones, and d) be freely available.

  Our platform continuously generates estimates of student ability by subject (using item-response theory) and in terms of content matter mastery levels (using cognitive diagnostic models). It is also set up for rapid A/B-testing through student-level experiments. We conceptualize these experiments as tools for continuous improvement, adapting the program as students engage with the platform. We will invite researchers through regular calls for proposals that benefit from Plio’s user base and will make de-identified click-level data openly available for external analysis.

### Automated Real Time Analysis of Online Tutoring Sessions

#### Team: Woot Math and Saga Education

  We propose to rapidly develop and deploy a new tool that could significantly accelerate the recovery of pandemic-related learning loss while also advancing the field of learning engineering. We will leverage and adapt recently developed natural language processing (NLP) and AI capabilities and existing instructional dialog processing pipelines to enhance the quality and scalability of high dose tutoring programs. Such programs are well established as uniquely effective in their ability to recapture learning losses. They also present unique opportunities for the application of learning engineering and what has been called “the double loop of learning” (see e.g., Feldstein, 2020) as illustrated in the figure below. The development of the proposed tool for the real time analysis of tutoring sessions will allow for tutors and their coaches to receive rapid high quality feedback on their practice and for program designers to receive detailed feedback on implemented materials. The availability of high quality and dense data from this system, along with the opportunities it presents for rapid, parallel experimentation, will in turn the field’s theories of learning and models of expertise.

### Closing the Continuous Improvement Loop with an Open Math Learning Engineering Platform for the 3rd-11th grade

#### Team: PolyUp

  PolyUp is a math and computer science learning platform designed to capture every step of mathematical thinking in a visual, executable form and create dynamic activities appropriate for each student’s level and aligned with 3rd-11th grade math standards. In Phase 1 and 2, over the past five years, PolyUp has developed, deployed, and tested various modules of a platform learning engineering architecture toward continuous improvement at scale. The proposed project covers Phase 3 to develop four of the 14 modules to close the loop in this system and deploy an Open Education Research Anonymized Data Lake (OERAD) for external researchers to analyze student and teacher behavior, generating relevant and diversified activities, and motivating asynchronous learning through the district, regional, national and global Challenges. OERAD will be developed to enhance the work of two US research partners (Stanford and WestED) and model the future of learning engineering research dissemination. Computational Thinking Alliance, a US non-profit, will be OERAD custodian to cater to researchers worldwide. OERAD will not only have Open API’s for obtaining and visualizing data for researchers, but it also will have an integrated dissemination platform to accelerate global education research collaboration.

  Four more organizations in Azerbaijan, Kenya, Rwanda, and Palestine are planning to develop their own research tools leveraging OERAD APIs. PolyUp equitable and scalable business model charges each country’s top 10% socioeconomic status population for Challenges and covers the other 90% with scholarships.

### ELENE-Experiential Learning Engineering Exchange

#### Team: Practera and Education Systems Center (Northern Illinois University and Northeastern University)

  The Experiential Learning Engineering Exchange (ELENE) seeks to improve the accessibility, quality and scalability of online work-based learning experiences. ELENE will provide an extensible set of analytics tools that will enable learning engineers and educators to collaboratively improve an existing and rapidly growing library of experiential learning programs (Experience Library). ELENE will build on the robust and established learning platform, Practera, which is used to deliver a wide range of experiential learning programs to tens of thousands of learners per year. ELENE will add the ability for Learning Engineers to use Practera’s rich learner data to improve the design of experiential learning programs. In addition, ELENE will give Learning Engineers the ability to build real-time analytic and intervention add-ons that instructors and coaches can use to improve their interactions with learners, or that learners/teams can use to monitor and reflect on their own performance and learning.

  ELENE will be piloted as part of the Illinois Work-Based Learning Innovation Network. With support from this award, we will aim to deploy the solution to high schools and community colleges across the State of Illinois and develop it as a model that could be deployed across all 50 states. There are already seven pilot sites signed up.

  We believe that, at scale, this solution could do for learning engineering what Kaggle has done for data science. It would create a place for aspiring learning engineers to learn and develop skills and for seasoned engineers to develop new insights into online experiential learning, it would raise awareness among educators of the benefits of learning engineering, and it would develop a shared library of robust work-based learning resources that can be easily adopted (and adapted) by educators.

### Family Engagement in the Digital Age

#### Team: Central Square Foundation

  The COVID-19 crisis has affected over 320M children in India from pre-primary to tertiary level due to school closures. It has further highlighted the learning crisis in India, where millions of children enter school with no school readiness skills. Moreover, lack of access to any education or resources is significantly higher for younger children and those from poorer backgrounds. During these times, parents can play a significant role to support children at home. Parental engagement is central to cognitive development in the early years ensuring that children are learning, surviving and thriving.

  The Top Parent app is a first of its kind solution that equips parents of low-income communities with high-quality EdTech resources, free of cost. Launched as a prototype to combat the COVID-19 crisis in April 2020, Top Parent has already acquired over a 140K downloads with ~30K Monthly Active Users, making it a robust database that is generating information across demographic and psychographic profiles; EdTech adoption and use across multiple apps; and parental behaviors. With over 150+ events captured through deep linking, API integration and data sharing agreements with partner EdTech apps, Top Parent has the potential to build a large body of evidence.

  We are seeking additional financial support to build out a learning engineering framework, upgrading our data analytic tools, and automating WhatsApp integration to enable high engagement with parents. Additionally, the award will support the project to collaborate with external researchers, and the EdTech solution providers to build a body of evidence that can help drive solutions and policy for large scale impact.

### Integrating UPchieve’s One-on-One Tutoring Into the ASSISTments Platform

#### Team: UPchieve and ASSISTments

  UPchieve is an edtech nonprofit that connects low-income students with free, live academic support any time they need it. UPchieve’s platform enables students to access high-quality tutoring and college counseling 24/7 from any device. ASSISTments is an online learning platform that is designed to make homework a more effective teaching and learning activity. ASSISTments simultaneously assists students as they complete homework and classwork, while providing teachers with useful assessments of student progress.

  For the Competition, we propose to integrate our two platforms so that eligible students using ASSISTments can seamlessly receive free, one-on-one tutoring on UPchieve when they get stuck. This integration will also involve linking our two datasets, which will have two primary benefits. First, UPchieve tutors will have additional context into what a student is working on and where they got stuck, enabling them to provide higher quality support to students. Second, knowing what question a student was working on prior to initiating a tutoring session will add crucial context to UPchieve’s tutoring chat logs. UPchieve’s chat log data would be of interest to many researchers using NLP to study math education, including Dr. Neil Heffernan.

### Kolibri: Data-driven Learning Models for Disconnected Populations

#### Team: Learning Equality

  This solution will enable learners to succeed in places with limited or no Internet connectivity during the COVID-19 pandemic and beyond. This will be supported through the Kolibri product ecosystem, an end-to-end suite of open-source tools, content, and DIY support materials, designed for offline teaching and learning. Data collected in these contexts will provide researchers with data from underserved and understudied populations—in terms of diversity of locations globally, linguistic and cultural backgrounds, and Internet connectivity—which are generally inaccessible to researchers, thereby ensuring that their learning needs are accounted for in academic research.

  To meet learners where they’re at and best support learning at home, we recognize the need to enable workflows for effective remote facilitation of learning by educators, while being responsive to limited parental support. To do this, the solution will build upon the existing functionality in Kolibri, such as supporting learner progress tracking, teacher support of students, and learning analytics, while enabling use of Kolibri away from the classroom environment. This will minimize learning loss in key academic subject areas, particularly Math, while fostering passion and curiosity for learning during the pandemic.

### Leveraging Technology to Democratize Assessment & Equip Families to Support Literacy at Home

#### Team: Springboard Collaborative

  According to research from Learning Heroes, 93% of K-8 parents believe their child is performing on grade level, despite the fact that teachers report less than a third of their students show up prepared for grade-level work. This imbalance in knowledge and power makes it exceedingly difficult for low-income parents to effectively advocate for their kids. Amidst closures, many schools have pared back or eliminated assessments altogether. As a result, teachers—like parents—are flying blind. They lack the data necessary to understand children’s individual learning needs and develop instructional plans that target those needs.

  Speech recognition technology holds the promise of automating literacy assessment. For teachers, this could yield more reliable data while reducing the loss of instructional time when administering classroom-based assessments. For parents, this could create visibility into their children’s reading development while providing actionable tips to support learning at home. We can’t, however, deliver on the promise of this technology, because speech recognition has not been optimized for young learners. This is especially true for children of color and English language learners.

  Springboard Collaborative is partnering with Dr. Neil Heffernan and the ASSISTments team to develop a Parent-Facing Literacy Screener (PFLS). Powered by speech recognition and AI, PFLS will automate literacy assessment such that it requires neither classroom time nor teaching expertise to administer. This tool will enable families to more deeply understand their children’s reading development, set goals, and measure progress. The data, in turn, will enable Springboard to personalize strategies that families and teachers can use to accelerate student learning. In order to build this technology, Springboard will capture and score recordings of children reading passages aloud. Neil’s team will, in turn, build algorithms that replicate these scores as closely as possible. By open-sourcing this demographically and linguistically diverse data set, we hope to advance the field of speech recognition to better meet the needs of marginalized children.

### NABU Inclusive Literacy Project

#### Team: NABU

  NABU’s platform targets the 40% of children (c. 250m) in developing countries who have virtually no access to books in a language they speak or understand, and who have been disproportionately affected by the COVID-19 pandemic. NABU is an award-winning low bandwidth reading app that caregivers can download for free and access with their children at home, designed for low cost smartphones that are widely available in developing countries. Through this project, we will transform our mobile reading app into a powerful learning engineering tool to measure the impact of culturally relevant, mother tongue reading materials on children’s intrinsic motivation to read, and literacy gains. This proposal builds on a substantial body of existing evidence across diverse settings that demonstrates that there are marked gains in literacy when children learn first in languages they speak and understand. Additional benefits include cognitive development, improved self-identity, increased access and equity, and stronger parental involvement in children’s education1. Our goal is to apply our learnings through fast-cycle iterative testing with NABU’s existing active user base of 25,000 children and caregivers in Rwanda, to accelerate their literacy gains, and provide a learning model that can scale quickly to support millions of children with the culturally appropriate resources they need to learn to read, on devices they already have access to at home.

### Using Engaging Games to Understand and Support Math Learning

#### Team: Learning Trajectories

  As families, caregivers, and teachers are being asked to do more to support their children’s learning, they need a tool that readily provides high quality learning activities tailored to their child’s needs. We propose to extend our existing platform with new games and reporting features. This will provide additional experiences tuned to the well-researched developmental progressions of learning math and assist adults in advancing that learning. While other platforms may provide experience with aspects of math, only the learning trajectories approach developed by Clements and Sarama addresses the cognitive development and learning-levels of math from birth to third grade- the most important period for a solid foundation in mathematics. Repeated experiences with games, as well as additional activities at the child’s appropriate level, will assist in solidifying metacognitive connections across contexts.

  The Learning and Teaching with Learning Trajectories (LTLT, or [LT]2) tool serve two purposes. First, it helps early childhood educators and caregivers learn about children's mathematical learning and provides a structure for tailoring learning experiences to children’s needs. Second, in 3 core topics, it assesses learning progress through online games and uses these assessment data to automatically move children through paths of learning. Although the research base for the content of this tool is strong, the site itself has not previously been used to collect data for the purpose of learning engineering. This proposal has three goals to further develop and scale this tool: (1) developing and refining new games for topics and levels of development that are currently not covered, and (2) developing new data collection procedures that generate reports and guidance for instructional decisions of teachers and families, as well new datasets for use by external researchers, (3) promoting the new games and new reports to engage new and existing users. This free online tool will help advance children’s early math learning in the context of the global pandemic by providing research-based, online, scalable math activities across many contexts.

### Worldreader Digital Reading Widget (WDRW)

#### Team: Worldreader

  Distance learning, and the role of parents and caregivers in supporting this learning, has become even more critical since the development of the COVID-19 pandemic. Worldreader will empower parents and caregivers by offering them the BookSmart Assessment, a formative reading assessment tool that guides their child aged 3-8 through a highly tailored collection of books based on the individual reading ability as tracked through the tool, enabling them to understand their child’s reading level and provide appropriate reading material. Moreover, the assessment tool can be embedded on partner web and Android apps through the development of a Worldreader Digital Reading Widget which will enable BookSmart to reach millions of children through the outreach of partners such as educational establishments, educational systems, and mobile phone networks. Free for end users, and available on low-tech phones with low data needs, this tool makes the gift of reading available to all.

  Building on Booksmart platform, which provides access to hundreds of books plus activities to support reading development, Worldreader will partner with Stanford Graduate School of Education to develop the formative reading assessment tool, and to analyze data collected through the tool. This will enable Worldreader to continuously improve tailoring of individual reading and learning journeys, and to demonstrate progression in reading skills, ultimately supporting children In grades K to P3 in the continual practice of reading which is fundamental to achieving early years learning outcomes.

