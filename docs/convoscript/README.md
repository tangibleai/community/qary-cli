# How to create dialog scripts

## Compose a new convesation thread

You can use any text editor, like Notepad or a Word Doc to plan out your dialog.

Conversation trees are made of several individual conversation *threads*.
If you can imagine one particular thread or path the conversation might take, you can record it in a script.
A script for the conversation is just a text file.
Do not use any formatting (bold, italics) if you are using Microsoft Word or another word processor.

You write these scripts as if you are writing the dialog in a screen play or novel.
Imagine what the bot would say and what the human might say in response. 
For example:

```yaml
Bot: How are you?
Human: Not great.
Bot: I'm sorry to hear that. Would you like to talk about it?
Human: Not right now.
Bot: OK, I understand. How 'bout those Mets, huh?
```

It's even more helpful if you let the dialog engine know where the separation is between each "turn" of the dialog.
A full turn of dialog is when the bot says something and the human responds, or vice versa.
It's best if you can separate each dialog turn with a hyphen on a line by itself:

#### _`data/writing/small-talk.script.yml`_
```yaml
-
  Bot: How are you?
  Human: Not great.
- 
  Bot: I'm sorry to hear that. Would you like to talk about it?
  Human: Not right now.
-
  Bot: OK, I understand. How 'bout those Mets, huh?
```

And if you think of multiple different things the bot or human might say, you can put all of those within the same dialog "turn".
For example:

#### _`data/writing/small-talk.script.yml`_
```yaml
-
  Bot: How are you?
  Human: Not great.
  Human: So so.
  Human: Com ci, com ca.
  Human: Hanging in there.
- 
  Bot: I'm sorry to hear that. Would you like to talk about it?
  Human: Not right now.
  Human: Not at the moment.
  Human: I'm fine. Can we talk about something else?
-
  Bot: OK, I understand. How 'bout those Mets, huh?
```

## Creating Text Files on GitLab

Now you can add your script to the qary package along side the [other data files that define dialog](https://gitlab.com/tangibleai/qary/-/edit/master/src/qary/data/writing/) for the chatbot.

To create a new dialog thread, or a branch in the dialog tree, you first copy the text from an existing dialog thread. 

Then hit the `+` (plus sign) button next to the file path near the top of gitlab:


> ![File name changed to `data/writing/french-small-talk.script.yml`](media/gitlab-plus-sign-create-new-file.png)

Later you can reread your scripts and look for oportunities to "branch" a thread by expanding on one of the things the human said a bit better.

Paste all of the previous thread into the new file.
Click the name of the file within a text box near the top of the page so your cursor is within the text box for the file name:

> ![Click on file name within path at top of page `qary/src/qary/data/writing/original-file-name.script.yml`](media/gitlab-rename-script-yaml-file.png)

You probably also want to change the file name to reflect what the new dialog thread is about, such as "french-small-talk.script.yml":

> ![File name changed to `data/writing/french-small-talk.script.yml`](media/gitlab-french-small-talk-file-name.png)

Then go back to the original file (script or conversation thread) and delete the human response that you are working on elaborating in the new thread.
The idea is to play the *one-of-these-things-is-not-like-the-others* game.
For example, the French response in the original dialog script can be deleted, if that's the thread you want to work on in the new file.
You can use the `#` (hash or pound symbol) to comment out or remove a line from the dialog script.
If you wanted to remove the French statement from the original dialog thread you would do it like this:

#### _`data/writing/small-talk.script.yml`_
```yaml
-
  Bot: How are you?
  Human: Not great.
  Human: So so.
#  Human: Com ci, com ca.
  Human: Hanging in there.
- 
  Bot: I'm sorry to hear that. Would you like to talk about it?
  Human: Not right now.
  Human: Not at the moment.
  Human: I'm fine. Can we talk about something else?
-
  Bot: OK, I understand. How 'bout those Mets, huh?
```

Then go back to the new file you created.
You might want to name the new file for the Go to the place where you want to create a new branch in the dialog.
You can "clean up" the new dialog by deleting all the other human responses in that dialog turn that don't apply for the new thread.
For example, you might want to delete all the English responses so you can focus the French dialog.

#### _`data/writing/french-small-talk.script.yml`_
```yaml
-
  Bot: How are you?
#  Human: Not great.
#  Human: So so.
  Human: Comme-ci, comme-ça.
#  Human: Hanging in there.
#  Human: Surviving.
#  Human: Just OK.
- 
  Bot: I'm sorry to hear that. Would you like to talk about it?
#  Human: Not right now.
#  Human: Not at the moment.
#  Human: I'm ok. Can we talk about something else?
-
  Bot: OK, I understand. How 'bout those Mets, huh?
```

No you want to fill that gap left by the English human responses that you deleted.
One easy way would be to use the yandex translator to create some French statements that are roughly equivalent to what an English speaker might have said. 

#### _`data/writing/french-small-talk.script.yml`_
```yaml
-
  Bot: How are you?
#  Human: Not great.
#  Human: So so.
  Human: Comme-ci, comme-ça.
#  Human: Hanging in there.
- 
  Bot: I'm sorry to hear that. Would you like to talk about it?
# Human: Not right now.  
  Human: Pas maintenant.
#  Human: Not at the moment.
  Human: Pas pour le moment.
#  Human: I'm ok. Can we talk about something else?
  Human: Je suis ok. Peut-on parler d'autre chose?
-
  Bot: OK, I understand. How 'bout those Mets, huh?
```

Now you can clean up all the comments, if you like. 
Though that is not required for the chatbot to understand your dialog script:

#### _`data/writing/french-small-talk.script.yml`_
```yaml
-
  Bot: How are you?
  Human: Comme-ci, comme-ça.
- 
  Bot: I'm sorry to hear that. Would you like to talk about it?
  Human: Pas maintenant.
  Human: Pas pour le moment.
  Human: Je suis ok. Peut-on parler d'autre chose?
-
  Bot: OK, I understand. How 'bout those Mets, huh?
```

And if you really want to be an over-achiever by having your bot use the same language/words as your user, you can adjust what the bot says, but only after the "split" in the original dialog tree, caused by the human's *one-of-these-things-is-not-like-the-others* response.

#### _`data/writing/french-small-talk.script.yml`_
```yaml
-
  Bot: How are you?
  Human: Comme-ci, comme-ça.
- 
  Bot: Je suis désolé de l'entendre. Aimeriez-vous en parler?
  Human: Pas maintenant.
  Human: Pas pour le moment.
  Human: Je suis ok. Peut-on parler d'autre chose?
-
  Bot: OK, je comprends. Comment 'bout ces Mets, hein?
```

And you may want to expand on all the ways a human in that "state" might say something similar:

#### _`data/writing/french-small-talk.script.yml`_
```yaml
-
  Bot: How are you?
  Human: Comme-ci, comme-ça.
  Human: Ça va.
  Human: Pas si grande.
- 
  Bot: Je suis désolé de l'entendre. Aimeriez-vous en parler?
  Human: Pas maintenant.
  Human: Pas pour le moment.
  Human: Je suis ok. Peut-on parler d'autre chose?
-
  Bot: OK, je comprends. Comment 'bout ces club de Marseille, hein?
```
