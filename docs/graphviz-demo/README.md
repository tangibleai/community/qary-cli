>>> from graphviz import Digraph
... 
... f = Digraph('finite_state_machine', filename='fsm.svg', format='svg')
... f.attr(rankdir='LR', size='64,50')
... 
... f.attr('node', shape='doublecircle')
... f.node('LR_0')
... f.node('LR_3')
... f.node('LR_4')
... f.node('LR_8')
... 
... f.attr('node', shape='circle')
... f.edge('LR_0', 'LR_2', label='SS(B)')
... f.edge('LR_0', 'LR_1', label='SS(S)')
... f.edge('LR_1', 'LR_3', label='S($end)')
... f.edge('LR_2', 'LR_6', label='SS(b)')
... f.edge('LR_2', 'LR_5', label='SS(a)')
... f.edge('LR_2', 'LR_4', label='S(A)')
... f.edge('LR_5', 'LR_7', label='S(b)')
... f.edge('LR_5', 'LR_5', label='S(a)')
... f.edge('LR_6', 'LR_6', label='S(b)')
... f.edge('LR_6', 'LR_5', label='S(a)')
... f.edge('LR_7', 'LR_8', label='S(b)')
... f.edge('LR_7', 'LR_5', label='S(a)')
... f.edge('LR_8', 'LR_6', label='S(b)')
... f.edge('LR_8', 'LR_5', label='S(a)')
... 
... f.view()
...
'fsm.svg.svg'
>>> %run src/qary/eda/visualizations.py
>>> %run src/qary/eda/dialog.py
>>> %run src/qary/etl/dialog.py
>>> from qary.etl import dialog
>>> from qary.etl.dialog import *
>>> who
>>> d1 = yaml.load(open('src/qary/data/writing/personal-essay-topic-selection-path-1.script.yml'), Loader=yaml.FullLoader)
>>> s1=d1; d1 = script_to_dialog(s1)
>>> yaml.dump(d1, open('d1.yml', 'w'))
>>> s1=d1; d1 = script_to_dialog('d1.yml')
>>> ls -al
>>> ls *.yml
>>> more d1.yml
>>> more d2.yml
>>> mv d1.yml s1.yml
>>> mv d2.yml s2.yml
>>> yaml.load(open('s1.yml'), Loader=yaml.FullLoader)
[{'student': 'Yup.',
  'teacher': 'So, you need to write a personal essay for your college application, huh?'},
 {'student': 'No.', 'teacher': 'Do you know what you’d like to write?'},
 {'student': 'I don’t know.',
  'teacher': 'Well, the people reading it want to understand you better. What do you want them to know about you?'},
 {'student': "That I'm a really awesome soccer player.",
  'teacher': 'Since this is a college application, the people reading it want to learn more about you, who you are. What do you want them to know about you?'},
 {'student': 'Yes.', 'teacher': 'So, you are passionate about soccer?'},
 {'student': 'Yes! I loved it!',
  'teacher': 'Great! Were you on the soccer team this year?'},
 {'student': 'Yes.',
  'teacher': 'Would you like the person reading your application to know what you\'ve learned from being part of soccer team -- like having a good attitude and working hard and cooperating with your teammates?"'},
 {'student': 'Okay.',
  'teacher': 'Then, let’s make that the focus of your personal essay, okay?'},
 {'student': 'Being on the soccer team has taught me a lot of things that are useful in life.',
  'teacher': 'So, let’s create a statement that captures that idea. Give it a try.'}]
>>> s1 = _
>>> s2 = yaml.load(open('s2.yml'), Loader=yaml.FullLoader)
>>> s1
[{'student': 'Yup.',
  'teacher': 'So, you need to write a personal essay for your college application, huh?'},
 {'student': 'No.', 'teacher': 'Do you know what you’d like to write?'},
 {'student': 'I don’t know.',
  'teacher': 'Well, the people reading it want to understand you better. What do you want them to know about you?'},
 {'student': "That I'm a really awesome soccer player.",
  'teacher': 'Since this is a college application, the people reading it want to learn more about you, who you are. What do you want them to know about you?'},
 {'student': 'Yes.', 'teacher': 'So, you are passionate about soccer?'},
 {'student': 'Yes! I loved it!',
  'teacher': 'Great! Were you on the soccer team this year?'},
 {'student': 'Yes.',
  'teacher': 'Would you like the person reading your application to know what you\'ve learned from being part of soccer team -- like having a good attitude and working hard and cooperating with your teammates?"'},
 {'student': 'Okay.',
  'teacher': 'Then, let’s make that the focus of your personal essay, okay?'},
 {'student': 'Being on the soccer team has taught me a lot of things that are useful in life.',
  'teacher': 'So, let’s create a statement that captures that idea. Give it a try.'}]
>>> s2
[{'student': "that's right",
  'teacher': 'So, you need to write a personal essay for your college application, huh?'},
 {'student': 'not really',
  'teacher': "Do you know have an idea for what you'd like to write about?"},
 {'student': "That I'm a good student and I really want to learn about robotics.",
  'teacher': 'Since this is a college application, the people reading it want to learn more about you, who you are. What do you want them to know about you?'},
 {'student': 'Yes! I loved it!',
  'teacher': "Excellent! Did you participate in this year's FIRST robotics competition?"},
 {'student': 'Yes.',
  'teacher': 'Would you like the person reading your application to know what you\'ve learned from being part of robotics teams -- like having a good attitude and working hard and cooperating with your teammates?"'},
 {'student': 'Okay.',
  'teacher': 'Then, let’s make that the focus of your personal essay, okay?'},
 {'student': 'The FIRST robotics competition has taught me a lot of things that are useful in life.',
  'teacher': 'So, let’s create a statement that captures that idea. Give it a try.'}]
>>> d1 = script_to_dialog(s1)
>>> d1 = script_to_dialog('s1.yml')
>>> d1 = script_to_dialog(open('s1.yml'))
>>> d1
[{'state': '__default__', 'nlp': {'__default__': 'exact'}, 'version': 2.0},
 {'state': 'turn_id_0',
  'bot': 'Yup.',
  'next_condition': {'turn_id_1': 'So, you need to write a personal essay for your college application, huh?'}},
 {'state': 'turn_id_1',
  'bot': 'No.',
  'next_condition': {'turn_id_2': 'Do you know what you’d like to write?'}},
 {'state': 'turn_id_2',
  'bot': 'I don’t know.',
  'next_condition': {'turn_id_3': 'Well, the people reading it want to understand you better. What do you want them to know about you?'}},
 {'state': 'turn_id_3',
  'bot': "That I'm a really awesome soccer player.",
  'next_condition': {'turn_id_4': 'Since this is a college application, the people reading it want to learn more about you, who you are. What do you want them to know about you?'}},
 {'state': 'turn_id_4',
  'bot': 'Yes.',
  'next_condition': {'turn_id_5': 'So, you are passionate about soccer?'}},
 {'state': 'turn_id_5',
  'bot': 'Yes! I loved it!',
  'next_condition': {'turn_id_6': 'Great! Were you on the soccer team this year?'}},
 {'state': 'turn_id_6',
  'bot': 'Yes.',
  'next_condition': {'turn_id_7': 'Would you like the person reading your application to know what you\'ve learned from being part of soccer team -- like having a good attitude and working hard and cooperating with your teammates?"'}},
 {'state': 'turn_id_7',
  'bot': 'Okay.',
  'next_condition': {'turn_id_8': 'Then, let’s make that the focus of your personal essay, okay?'}},
 {'state': 'turn_id_8',
  'bot': 'Being on the soccer team has taught me a lot of things that are useful in life.',
  'next_condition': {'turn_id_9': 'So, let’s create a statement that captures that idea. Give it a try.'}}]
>>> import qary.eda.visualizations
>>> qary.eda.visualizations.graph_dialog_turns?
>>> qary.eda.visualizations.graph_dialog_turns(d1)
<graphviz.dot.Digraph at 0x7f069c765400>
>>> g = _
>>> with open('d1.svg', 'w') as fout:
...     fout.write(dumps(g))
...
>>> ls
>>> !firefox d1.svg
>>> history
>>> history
>>> history -o -p
>>> history ~1
>>> history ~1/1-1000
>>> history -o -p -f graphviz_demo.md
```
