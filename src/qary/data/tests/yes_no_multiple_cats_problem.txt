# state: __WELCOME__

Bot: A woman went on vacation and asked a friend to look after her cat. A week later, 8 grown cats were living in the apartment.

Bot: Let's see how fast you can solve this mystery by asking me questions. The fewer questions you ask the better player you are. 

Bot: Remember: I can only answer with "yes" or "no". Or, sometimes. with "irrelevant". 

Bot: Let's play! Ready? 

Player: yes

Bot: Great! Ask me your first question. 

Bot: Remember: each question should bring you closer to solving the puzzle.

# state: Play
Player: Did she already have other cats? 

Bot: No

Player: Did the cat go missing and have babies and came back? 

Bot: It appears that your question consists of multiple questions. Can you break that question up into different questions? 

Player: Did her cat go missing? 

Bot: Yes

Player: Did her cat have babies? 

Bot: And here is a little tip: If you carefully read an initial problem statement, then you'll need fewer questions to solve it. See "grown cats" in the initial statement

Player: Did the cat come back with other cats? 

Bot: No

Player: They heard that he was a great caretaker and came to him? 

Bot: No

Player: Did the caretaker feel bad so he got other cats to cheer her up? 

Bot: No

Player: Did he feel bad for losing her cat so he decided to get 8 other different cats for to choose from? 

Bot: No

Player: Did the caretaker bring these 8 cats to the apartment? 

Bot: Yes. 

Player: Did he bring them all at once? 

Bot: Irrelevant

Player: Tbh, this is unfair. Why is it irrelevant? 

Bot: I'm sorry. Here is a tip: when one line  of enquiry reaches an end then another approach is needed, often from a completely new direction. This is where the lateral thinking in. This types of puzzles are also called lateral thinking puzzles

Player: Ok. Did the man bring those cats to the apartment on purpose? 

Bot: Yes. 

Bot: You're getting closer! 

Player: Did he bring them for the lady? 

Bot: Yes. 

Player: Did he bring them for the lady to choose from? 

Bot: Yes

Player: I solved it!

Bot: Not yet. 

Player: Not fair! The man brought other cats so that the lady could choose a cat to 
replace the missing me. 

Bot: Please ask me a question. 

Player: Did the man bring other cats so that the lady could choose a cat to 
replace the missing one? 

Bot: No

Player: I don't understand 

Bot: This kind of puzzle teaches you to check your assumptions about any situation. You need to be open-minded, flexible and creative in your questioning and able to put lots of different clues and pieces of information together. Once you reach a viable solution you keep going in order to refine it or replace it with a better solution. This is lateral thinking! 

Player: Is it important what kind of cats they were?

Bot: Yes. 

Player: Were they stray cats or did he steal them from their owners? 

Bot: Please ask me a question that I can answer with "yes", "no" or "irrelevant".

Player:  Did he steal those cats? 

Bot: No

Player: Stray cats? 

Bot: Yes.

Player: There was something special about them? 

Bot: Yes.

Player: Was it breed? 
	
Bot: Irrelevant

Player: I give up!

Bot: You're so close!

Bot:  If you want to quit and save your progress reply with "q" anytime!

Player: Did these cats look like the missing one?

Bot: Yes.

Player: Did he think that the missing cat could be among them? 

Bot: Yes.

#state: finish
Player: Did he think that the lady would recognize her cat among these 8 cats? 

Bot: Yes.

Bot: Kudos! You solved it!

Bot: The next day this cat ran away, and the friend had to start looking for it. Since he did not know the cat very well, he had to keep all the similar cats that he had seen and wait for the friend who was supposed to identify the right pet.
 
Bot: You solved this puzzle with 22 questions.  You're in top 30% of the "8 cats" puzzle conquerors! Most of the players used more than 22 questions to solve this puzzle!

Bot: Keep training your lateral thinking and deductive logic skills! I have tons of puzzles for you. Type "m" if you'd like to crack another one.


