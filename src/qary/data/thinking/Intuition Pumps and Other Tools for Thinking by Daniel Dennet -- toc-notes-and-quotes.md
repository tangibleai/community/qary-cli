# Intuition Pumps and Other Tools for Thinking by Daniel C. Dennett

## I. Introduction: What Is an Intuition Pump?


## II. A Dozen General Thinking Tools 17

### 1. Making Mistakes 19

### 2. "By Parody of Reasoning": Using Reductio ad Absurdum 29

### 3. Rapaport's Rules 33

### 4. Sturgeon's Law 36

### 5. Occam's Razor 38

### 6. Occam's Broom 40

### 7. Using Lay Audiences as Decoys 42

### 8. Jootsing 45

### 9. Three Species of Goulding: Rathering, Piling On, and the Gould Two-Step 48

### 10. The "Surely" Operator: A Mental Block 53

### 11. Rhetorical Questions 55

### 12. What Is a Deepity? 56


## III. Tools for thinking about Meaning or Content 59

### 13. Murder in Trafalgar Square 61
Imagine people in Moscow and London reading a news article about a Frenchman shooting and killing a man in Trafalgar square.
Each person will have a different understanding of the event, including the Frenchman and the police officers involved.
Different languages may be used but the words will create some shared understanding that a Frenchman caused the death of someone at a famous location in London.
The patterns or encodings of this knowledge in their brains will be vastly different in the sense of the kinds of neurons that are involved and the connections that are created with other memories in each brain.
So brains do not "encode" or translate knowledge into some universal "brainish" that is the same everywhere.
And even if everyone uses a common language like English to share knowledge, the unique perspectives of each person will create unique understandings and knowledge in each brain.
It doesn't matter how precise and simple and rigorously or formally accurate the language is, it will have a different affect on each brain.

### 14. An Older Brother Living in Cleveland 65

### 15. "Daddy Is a Doctor" 68

### 16. Manifest Image and Scientific Image 69
Two views on the table in front of you, the hard surface with your hand pushing against it and the vast empty space between nanometer size atoms and particles.

### 17. Folk Psychology
Common Sense

### 18. The Intentional Stance 77

### 19. The Personal/Sub-personal Distinction 86

### 20. A Cascade of Homunculi 91

### 21. The _Sorta_ Operator 96
When you analyze and synthesize a thing, to slot it into your brain's neurons and create a building block, you have to keep track of the two things about each level, what a thing _is_ and what it _does_.
"We need Darwin's gradualism to explain the huge difference between an ape and an apple, and we need Turing's gradualism to explain the huge difference between a humanoid robot and a hand calculator. The ape and the apple are made of the same basic ingredients, differently structured and exploited in a many-level cascade of different functional competences. There's no principled dividing line between a sorta ape and an ape. We use the **intentional stance** to keep track of the beliefs and desires (of "beliefs" and "desires" or sorta beliefs and sorta desires) of the (sorta-) rational agents at every level from the simplest bacterium through all the discriminating, signaling, comparing, remembering circuits that comprise the brains of animals from starfish to astronomers. There is no principled line above which true comprehension is to be found--even in our own case. The small child sorta understands her own sentence "Daddy is a doctor," and I sorta understand `E = m*c^2.` Some philosophers resist this anti-essentialism (see chatper 43): either you believe that snow is white or you don't; either you are conscious or you aren't; nothing couns as an approximation of any mental phenomenon; it's all or nothing. And to such thinkers, the power of midns are insoluble mysteries because minds are 'perfect,' and perfectly unlike anything to be found in mere material mechanisms."
"

### 22. Wonder Tissue 98

### 23. Trapped in the Robot Control Room 102



## IV. An Interlude About Computers 107

### 24. The Seven Secrets of Computer Power Revealed 109

### 25. Virtual Machines 133

### 26. Algorithms 140

### 27. Automating the Elevator 143


### Summary (of an interlude about computers) 148


## V. MOre Tools About Meaning 151

### 28. A Thing about Redheads 153

### 29. The Wandering Two-Bitser, Twin Earth, and the Giant Robot 157

### 30. Radical Translation and a Quinian Crossword Puzzle

### 31. Semantic Engines and Syntactic Engines 178

### 32. Swampman Meets a Cow-Shark 180

### 33. Two Black Boxes 184


### Summary (of more tools about meaning) 197


## VI. Tools for Thinking about Evolution

### 34. Universal Acid 203

### 35. The Library of Mendel: Vast and Vanishing 205

### 36. Genes as Words or as Subroutines 214

### 37. The Tree of Life 217

### 38. Cranes and Sky hooks, Lifting in Design Space 218

### 39. Competence without Comprehension 232

### 40. Fee-Floating Rationales 234

### 41. Do Locusts Understand Prime Numbers? 236

### 42. How to Explain Stotting 238

### 43. Beware of the Prime Mammal 240

### 44. When Does Speciation Occur? 244

### 45. Widowmakers, Mitochondrial Eve, and Retrospective Coronations 247

### 46. Cycles 252

### 47. What _Does_ the Frog's Eye Tell the Frog's Brain? 256

### 48. Leaping through Space in the Library of Babel 258

### 49. Who Is the Author of _Spamlet_? 260

### 50. Noise in th Virtual Hotel 267

### 51. Herb, Alice, and Hal, the Baby 271

### 52. Memes 274


### Summary (Tools for Thinking about Evolution) 352


## VII. Tools for Thinking about Consciousness 297

### 53. Two Counter-images 281

### 54. The Zombic Hunch 283

### 55. Zombies and Zimboes 288

### 56. The Curse of the Cauliflower 296

### 57. Vim: How Much is that in "Real MOney"? 299

### 58. The Sad Case of Mr. Clapgras 302

### 59. The Tuned Deck 310

### 60. The Chinese Room 319

### 61. The Teleclone Fall from Mars to Earth 330

### 62. The Self as the Center of Narrative Gravity 333

### 63. Heterophenomenology 341

### 64. Mary the Color Scientist: A Boom Crutch Unveiled 347


### Summary (Tools for Thinking about Consciousness) 352


## VII. Tools for Thinking about Free Will 355


### 65. A Truly Nefarious Neurosurgeon 357

### 66. A Deterministic Toy: Conway's Game of Life 359

### 67. Rock, Paper, Scissors 370

### 68. Two Lotteries 375

### 69. Insert Historical Facts 378

### 70. A Computer Chess Marathon 384

### 71. Ultimate Responsibility 393

### 72. Sphexishness 397

### 73. The Boys from Brazil: Another Boom Crutch 401


### Summary (VII. Tools for Thinking about Free Will) 406


## IX. What is it Like to Be a Philosopher? 409


### 74. A Faustian Bargain 411

### 75. Philosophy as Naive Auto-anthropology 414

### 76. Higher-Order Truths of Chmess 418

### 77. The 10 Percent That's Good 425


## X. Use the Tools. Try Harder. 429


## XI. What got Left Out 431


### Appendix: Solutions to Register Machine Problems 433

### Sources 445

### Bibliography 451

### Credits 461

### Index 463

## References

[^1]: _Consciousness Explained_ by Daniel C. Dennet
