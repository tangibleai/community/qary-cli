tags: [Python, Machine Learning, Interview, NLP]

Q1: Given two strings, string1 and string2, write a function str_map to determine if there exists a one-to-one correspondence between the characters of string1 and string2.
hint: The easiest way to solve this problem is to check conditions that make a bijection between string characters impossible. We return ‘False’ if our strings fit a condition, and ‘True’ otherwise.

Q2: |+
  Write a function shortest_transformation to find the length of the shortest transformation sequence from begin_word to end_word through the elements of word_list. 
  Note: Only one letter can be changed at a time and each transformed word in the list must exist.

Q3: |+
  Write a function given the formatted list of dictionaries above to return a list of matches along with scheduled times.

  You're given a list of people to match together in a pool of candidates.

  We want to match up people based on two ways:

    A hard filter on scheduled availability
    A closest match based on similar interests

  In this scenario, we would return a match of Bob and Joe while also matching Carolyn and Dan. Even though Carolyn and Dan don’t have any interest overlap, Carolyn is the only one with availability to meet Dan’s schedule.

  The goal is to optimize the total number of matches first while then subsequently optimizing on matching based on interests.

Hint: You can use the Blossom graph search algorithm - given a graph, it returns the largest weighted number of edges where no vertex is included more than once.

Q4: Given a dictionary with keys of letters and values of a list of letters, write a function closest_key to find the key with the input value closest to the beginning of the list.
hint: "Consider that a signed 'difference' or 'displacement' or 'delta' is a different metric than 'distance'. Is your computed distance always positive? Negative values for distance (for example between 'c' and 'a' instead of 'a' and 'c') will interfere with getting an accurate result."

Q5: Given two strings, string1 and string2, write a function max_substring to return the maximal length substring shared by both strings.

Q6: |+
  Build a K-nearest neighbors classification model from scratch with the following conditions:

  Use Euclidean distance (aka, the “2 norm”) as your closeness metric
  Your function should be able to handle data frames of arbitrary many rows and columns
  If there is a tie in the class of the k nearest neighbors, rerun the search using k-1 neighbors instead
  You may use pandas and numpy but NOT scikit-learn

    Example:

    ```ipython
    >>> k = 5
    >>> dataframe = pd.read_csv('knn_example_dataset.csv')
    >>> dataframe
          Var1    Var2   Var3      Target
      0  -3.279  3.362  2.847       2
      1  -0.791  1.742  2.151       2
      2  -0.785 -0.938 -0.459       0
      3  -1.068  1.461  0.127       3
      4  -0.367= -0.87 -0.225       0
      ..       ...       ...       ...     ...
      95 -1.327  1.971 -0.690       2
      96 -3.203  1.847  0.778       2
      97 -0.587  0.647  2.094       2
      98  0.363 -0.509  2.514       1
      99 -0.673  2.955  2.102       4
      [100 rows x 4 columns]

    >>> unlabeled_example = [0.5, -2, 8]
    >>> my_knn(dataframe, k, unlabeled_example)
    2
    >>> validation_dataset = pd.read_csv('knn_validation_dataset.csv')
    >>> for i, x in validation_dataset.values[:,0:-1]:
    ...     print(i, my_knn(x))
    ```

  Your goal is to achieve accuracy at least as good as a scikit-learn model on an unknown set of examples that we will run your function on.
