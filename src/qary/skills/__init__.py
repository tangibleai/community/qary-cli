__all__ = [s.strip() for s in 'base, faq, eliza, life, poly, pulse, spelling, yes_no'.split(',')]


from qary.skills import base, faq, eliza, life, poly, pulse, spelling, yes_no  # noqa
