import os
import logging
import shutil
import tempfile

from qary.constants import DATA_DIR
from qary.skills import quiz
from qary.etl.utils import squash_wikititle as normalize_text
from qary.chat.v2 import WELCOME_STATE_NAME

log = logging.getLogger('qary')


__author__ = "SEE AUTHORS.md"
__copyright__ = "Jose Robins"
__license__ = "The Hippocratic License, see LICENSE.txt (MIT + Do no Harm)"


class TestQuiz:
    """Tests to verify the yes_no.py module """

    test_quiz_path = os.path.join(DATA_DIR, 'testsets/dialog/intern_quiz.yml')
    test_quiz_dialog_path = os.path.join(DATA_DIR, 'testsets/dialog_parser.input.dialog.yml')
    log.error(test_quiz_dialog_path)

    def setup_class(self):
        pass

    def test1_welcome_state(self):
        """Tests the very first turn which puts the bot into welcome state """
        skill = quiz.Skill(datafile=self.test_quiz_path)
        str_expected = (
            'I\'m going to ask you a few questions about python to set a baseline on your python knowledge.'
            'This way you can see how much you learn over time.'
            'So don\'t worry if you don\'t know the answers, you will soon!'
            'Ready?'
        )
        str_expected = normalize_text(str_expected)
        responses_expected = [(1.0, str_expected)]
        responses_actual = skill.reply(None)
        str_actual = responses_actual[0][1]
        str_actual = normalize_text(str_actual)
        responses_actual = [(responses_actual[0][0], str_actual)]
        assert responses_actual == responses_expected
        assert skill.state == WELCOME_STATE_NAME

    def test2_quiz_start_reponse_state(self):
        """Tests the 2nd turn to ensure that the player's affirmative response is accepted  """
        skill = quiz.Skill(datafile=self.test_quiz_path)
        str_start_expected = 'Rate your \'python\''
        str_start_expected = normalize_text(str_start_expected)
        player_reply = 'yes'
        state_expected = 'q0'
        skill.reply(None)
        responses_actual = skill.reply(player_reply)
        str_actual = responses_actual[0][1]
        str_actual = normalize_text(str_actual)
        substr_pos = str_actual.find(str_start_expected)
        assert substr_pos == 0
        assert skill.state == state_expected

    def test3_quiz_part_way_reponse_state(self):
        """Tests the 3nd turn to ensure that things are chugging along smoothly  """
        skill = quiz.Skill(datafile=self.test_quiz_path)
        str_start_expected = 'Name one of your'
        str_start_expected = normalize_text(str_start_expected)
        player_replies = [None, 'yes', 'dummy']
        state_expected = 'q1'
        for reply in player_replies:
            responses_actual = skill.reply(reply)
        # noinspection PyUnboundLocalVariable
        str_actual = responses_actual[0][1]
        str_actual = normalize_text(str_actual)
        substr_pos = str_actual.find(str_start_expected)
        assert substr_pos == 0
        assert skill.state == state_expected

    # FIXME: next_condition = {} or None fail to terminate the bot
    def test4_quiz_finish(self):
        """Make sure there's an end to the world as we know it (None state)"""
        skill = quiz.Skill(datafile=self.test_quiz_path)
        player_replies = [None, 'yes']
        player_replies.extend(['foo'] * 9)
        for reply in player_replies:
            print(reply)
            responses_actual = skill.reply(reply)
            print(responses_actual)
        assert 'quit' in responses_actual[0][1].lower()
        # noinspection PyUnboundLocalVariable
        str_actual = responses_actual[0][1]
        str_actual = normalize_text(str_actual)
        substr_pos = str_actual.find(normalize_text('Session is already over'))
        assert substr_pos == 0
        assert skill.state is None

    def test5_non_existent_datafile(self):
        """Tests a non existent data file"""
        data_file = os.path.join(DATA_DIR, 'testsets/dialog/non_existent_data.yml')
        skill = quiz.Skill(datafile=data_file)
        assert skill.turns_input is None

    def test6_wrong_format_datafile(self):
        """Tests a non existent data file"""
        with tempfile.TemporaryDirectory() as tmp_dir:
            data_file_wrong_ext = os.path.join(tmp_dir, 'intern_quiz.json')
            shutil.copy(self.test_quiz_path, data_file_wrong_ext)
            skill = quiz.Skill(datafile=data_file_wrong_ext)
            assert skill.turns_input is None

    def test7a_dialog_parser_next_state_exact(self):
        """Tests the next state capability"""
        skill = quiz.Skill(datafile=self.test_quiz_dialog_path)
        response = skill.reply(None)
        assert skill.state == 'welcome_state'
        response = skill.reply('Q2_exact_response')
        assert skill.state == 'q2_exact'
        # now test the negative condition and ensure this goes through to the fall through_state
        skill.state = None
        response = skill.reply(None)
        response = skill.reply('q2_exact_response')  # lower case q2 should fail
        assert skill.state == '__finish__'
        assert response[0][0] == 1.0
        assert response[0][1] == 'Goodbye'

    def test7b_dialog_parser_next_state_lower(self):
        """Tests the next state capability"""
        skill = quiz.Skill(datafile=self.test_quiz_dialog_path)
        responses = skill.reply(None)
        assert skill.state == 'welcome_state'
        responses = skill.reply('q6_lower_response')  # intent is specified with upper case Q6
        assert skill.state == 'q6_lower'
        # now test with uppercase; it should still go through
        skill.state = None
        responses = skill.reply(None)
        responses = skill.reply('Q6_LOWER_RESPONSE')  #
        assert responses[0][0] == 1.0
        assert skill.state == 'q6_lower'

    def test7c_dialog_parser_next_state_case_sensitive_kw(self):
        """Tests the next state capability"""
        skill = quiz.Skill(datafile=self.test_quiz_dialog_path)
        response = skill.reply(None)
        assert skill.state == 'welcome_state'
        response = skill.reply('Q4_case_keyword_response')  # proper response
        assert skill.state == 'q4_case_sensitive_keyword'
        # now test with incorrect case; it should go to the fall through case
        skill.state = None
        response = skill.reply(None)
        response = skill.reply('q4_case_keyword_response')
        assert response[0][0] == 1.0
        assert skill.state == '__finish__'

    def test7d_dialog_parser_next_state_keyword(self):
        """Tests the next state capability"""
        skill = quiz.Skill(datafile=self.test_quiz_dialog_path)
        response = skill.reply(None)
        assert skill.state == 'welcome_state'
        response = skill.reply('q3_keyword_response')  # proper response
        assert skill.state == 'q3_keyword'
        # now test with uppercase
        skill.state = None
        response = skill.reply(None)
        response = skill.reply('Q3_keyword_reponse')  #
        assert skill.state == 'q3_keyword'
        assert response[0][0] == 1.0

    def test7e_dialog_parser_next_state_normalize(self):
        """Tests the next state capability"""
        skill = quiz.Skill(datafile=self.test_quiz_dialog_path)
        responses = skill.reply(None)
        assert responses[0][0] == 1.0
        assert skill.state == 'welcome_state'
        responses = skill.reply('Yes! ')
        assert responses[0][0] == 1.0
        assert skill.state == 'q1_normalize'

    def test8_quiz_via_clibot(self):
        from qary.clibot import CLIBot

        # , skill_init_kwargs_list = [{'datafile': self.test_quiz_dialog_path}])
        bot = CLIBot(skill_module_names=['quiz'],
                     skill_init_kwargs_list=[dict(datafile=self.test_quiz_dialog_path)])
        response = bot.reply(None)
        assert len(response) >= 1
        assert bot.skills[0].state == 'welcome_state'
        assert response[1][:66] == "Welcome\nI will ask you a few Python questions. Please answer 'yes'"
        response = bot.reply('Yes! ')
        assert len(response) >= 1
        assert(response[1] == 'Name the two fundamental numerical data types in Python')
        assert bot.skills[0].state == 'q1_normalize'
        response = bot.reply('foo')
        assert len(response) >= 1
        assert bot.skills[0].state == 'q2_exact'
